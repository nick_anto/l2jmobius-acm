<?php
if (!defined('l2jmobius')) {
    die('Direct access not permitted');
}
$locales = array(
	'en'=>array('name'=>'English', 'code'=>'en_GB'),
	'el'=>array('name'=>'Ελληνικά', 'code'=>'el_GR'),
	'ru'=>array('name'=>'Русский', 'code'=>'ru_RU')
);