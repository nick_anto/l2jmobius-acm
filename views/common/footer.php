<?php
if (!defined('l2jmobius')) {
    die('Direct access not permitted');
}
?>
		<footer class="sticky-footer bg-white">
			<div class="container my-auto">
				<div class="copyright text-center my-auto">
					<span>Copyright &copy; <?=date('Y').' - '.$appName;?></span>
				</div>
			</div>
		</footer>