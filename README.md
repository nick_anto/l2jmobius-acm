
# L2jMobius ACM - Account Manager for L2jMobius

L2jMobius ACM is a comprehensive account manager specifically designed for the [L2jMobius](https://l2jmobius.org/) platform, an open-source Java emulator for the popular MMORPG Lineage 2. This tool streamlines the account management process, providing a user-friendly interface and efficient functionalities tailored for both administrators and players of [L2jMobius](https://l2jmobius.org/).


## Live Demo

Demo url: (https://l2jmobius-acm.lineage2arc.com)

Admin account:
```
username: admin
password: 123456
```

Test Paypal account for trying out a donation:
```
email: sb-wlzxe29027028@personal.example.com
password: VUOe#55H
```

Test credit card for trying out a donation using Stripe:
```
Card number: 4242424242424242
Card Holder: Test Test
CVV: 111
Expiry: 11/28
```


## Screenshots

![Login Page](https://i.postimg.cc/hvpX8G5n/login.jpg)

![Settings](https://i.postimg.cc/JzgBdG44/settings.jpg)

View [more screenshots](docs/screenshots.md).


## Installation on Debian

Read the [Installation on Debian](docs/installation-on-debian.md) guide.


## Installation on Windows Using XAMPP

Read the [Installation on Windows](docs/installation-on-windows.md) guide.


## FAQ

Read the [F.A.Q.](docs/faq.md) doc.

## Support

For support, use the L2jMobius forum and reply to this topic: https://l2jmobius.org/forum/index.php?topic=11466.0


## Contributing to L2jMobius ACM

Read the [Contributions](docs/contributions.md) doc.


## Important Notice: Trust Only the Official Repository

I've noticed that there are copies of this project's source code that have been altered and redistributed on various forums and websites. I strongly advise that you **only trust this official GitLab repository** for downloading the source code to ensure its integrity and security.

Downloading and using versions of this software from unofficial sources may pose security risks and could contain modifications that are not approved or verified by the original developers. To ensure you have the most reliable and secure version of the software, please use only the code hosted in this repository.

Example of unauthorized (and probably altered) distribution which you should stay away from: https://mmo-dev.info/threads/l2jmobius-acm-an-account-management-and-donations-panel.28105/

## License

L2jMobius ACM is released under the MIT License. This means that the project is completely free for personal, educational, or commercial use.

Copyright (c) 2024 L2jMobius ACM

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Credits

This project makes use of several open source software and libraries. We acknowledge and are grateful to these developers for their contributions to the open source community.

- **[Bootstrap](https://getbootstrap.com/)**: Used for designing and customizing responsive mobile-first sites.
- **[jQuery](https://jquery.com/)**: Employed for simplifying HTML DOM tree traversal and manipulation, as well as event handling, CSS animation, and Ajax.
- **[Font Awesome](https://fontawesome.com/)**: Integrated for its vast collection of icons and social logos for web projects.
- **[SB Admin 2](https://startbootstrap.com/theme/sb-admin-2)**: This theme, available at Start Bootstrap, has been used as the basis for our project's dashboard design.
- **[phpdotenv](https://github.com/vlucas/phpdotenv)**: An open source library to read env data.
- **[PHPMailer](https://github.com/PHPMailer/PHPMailer)**: An open source full-featured email creation and transfer class for PHP.
- **[php-markdown](https://github.com/michelf/php-markdown)**: This is a library package that includes the PHP Markdown parser and its sibling PHP Markdown Extra with additional features.
- **[L2JMobius](https://l2jmobius.org/)**: The game's database structure is based on L2JMobius.

Each of these tools has been instrumental in the development of our project, and we are immensely thankful for the work of their creators and maintainers.
